# Feature 7 - LoginView Step 5 define urls pattern
from django.urls import path
from . import views
from .views import logout_view

urlpatterns = [
    path("login/", views.login_view, name="login"),
    # Feature 9 - Logout step 2 In the accounts/urls.py, register that view in your urlpatterns list
    # Feature 9 - Logout step 2 with the path "logout/" and the name "logout"
    path("logout/", logout_view, name="logout"),
]
