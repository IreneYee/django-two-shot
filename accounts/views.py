from django.shortcuts import render

# Feature 7 - LoginView Step 3 Create a Login View Function
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect

# Feature 7 - LoginView Step 3 Create a Login View Function
from .forms import LoginForm

# Feature 9 - Logout step 1 Create a function in accounts/views.py that logs a person out then redirects them
# to the URL path registration named "login"
from django.contrib.auth import logout


# Feature 7 - LoginView Step 3 Create a Login View Function
def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = LoginForm()
    return render(request, "accounts/login.html", {"form": form})


# Feature 9 - Logout step 1 Create a function in accounts/views.py that logs a person out then redirects them
# to the URL path registration named "login
def logout_view(request):
    logout(request)
    return redirect("login")
