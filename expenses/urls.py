"""
URL configuration for expenses project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin

# Feature 5: step 4 Include the URL patterns from the receipts app in the expenses project
from django.urls import path, include


# Feature 6 - Home: Redirecting the main page
from receipts.views import redirect_to_receipt_list

urlpatterns = [
    path("admin/", admin.site.urls),
    # Feature 5: step 4 with the prefix "receipts/"
    path("receipts/", include("receipts.urls")),
    # Feature 6 - Home: Redirecting the main page
    path("", redirect_to_receipt_list, name="home_page"),
    # Feature 7 - LoginView Step 6 include accounts urls in the main project
    path("accounts/", include("accounts.urls")),
]
