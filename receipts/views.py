from django.shortcuts import render
from .models import Receipt


# Feature 6 - Home: Redirecting the main page
from django.shortcuts import redirect

# Feature 8 - step 1 Protect the List View
from django.contrib.auth.decorators import login_required


# Feature 5: step 1# Create a view that will get all of the instances of the Receipt model
# Feature 8 - step 1 Protect the List View
@login_required
def receipt_view(request):
    # Feature 8 - step 2 Filter the Queryset:
    # puchaser is from the model ForeignKey
    receipts = Receipt.objects.filter(purchaser=request.user)
    # Feature 5: step 1# and put them in the context for the template.
    context = {"receiptsView": receipts}
    # we are calling the html file inside the folder receipts/details file
    return render(request, "receipts/detail.html", context)


# Feature 6 - Home: Redirecting the main page
def redirect_to_receipt_list(request):
    return redirect("/receipts/")
