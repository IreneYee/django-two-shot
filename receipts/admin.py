from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt


# Feature 4 - Admin: Register the three models with the admin
# Feature 4 - Admin: so that you can see them in the Django admin site.
@admin.register(ExpenseCategory)
class ExpenseAdmin(admin.ModelAdmin):
    list_display = [
        "name",
    ]


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ["name", "number", "owner"]


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = ["vendor", "total", "tax", "date", "category", "account"]
