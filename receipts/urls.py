from django.urls import path


# Feature 5: step 2 # in a new file named receipts/urls.py.
# Feature 5: step 2 #Register that view in the receipts app
from receipts.views import receipt_view

# Feature 5: step 2 for the path ""
# Feature 5: step 2 and the name "home"

# calling receipt_view from the function in view.py
urlpatterns = [
    path("", receipt_view, name="home"),
]
